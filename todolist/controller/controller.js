function layThongTinTuForm() {
    const tenCongViec = document.getElementById("newTask").value;
    return new CongViec(tenCongViec);
}

function renderDSCV(cvArr) {
    var contentHTML = "";
    for (var i = 0; i < cvArr.length; i++) {
        var cv = cvArr[i];
        var trContent = `
        <li>
                <span>${cv.tenCv}</span>
                <a class="text-warning" style="cursor:pointer" data-target="#myModal" data-toggle="modal">
                
                
                 <i class="fa-solid fa-trash-can" <button onclick="xoaCongViec('${cv.tenCv}')" ></button></i>
                <span class="bg-success text-white">
                 <i class="fa-solid fa-circle-check" <button onclick="danhDauDaLam('${cv.tenCv}')"></button></i>
                </a>

            </li>
        `;
        contentHTML += trContent;
    }
    document.getElementById("todo").innerHTML = contentHTML;
}


function timKiemViTri(id, dscv) {
    for (var index = 0; index < dscv.length; index++) {
        var cv = dscv[index];
        if (cv.tenCv == id) {
            return index;
        }
    }

    return -1;
}

function timKiemViTri2(ten, dscvHoanThanh) {
    for (var index = 0; index < dscvHoanThanh.length; index++) {
        var cv = dscvHoanThanh[index];
        if (cv.tenCv == ten) {
            return index;
        }
    }

    return -1;
}






function showDaHoanThanh(cvArr) {
    var contentHTML = "";
    for (var i = 0; i < cvArr.length; i++) {
        var cv = cvArr[i];
        var trContent = `
          <li>
        <span class="text-success">${cv.tenCv}</span>
            <a class="text-warning" style="cursor:pointer" data-target="#myModal" data-toggle="modal">
                <span class="bg-success text-white">
                <i class="fa-solid fa-circle-check" onclick=danhDauDaLam('')></i>
                </span>
            </a>
        </li>
        `;
        contentHTML += trContent;
    }
    document.getElementById("completed").innerHTML = contentHTML;
}
