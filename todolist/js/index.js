const DSCV_LOCALSTORAGE = "DSCV_LOCALSTORAGE";
const DSCV_LOCALSTORAGE2 = "DSCV_LOCALSTORAGEE";

var dscv = [];

var dscvHoanThanh = []

var dscvJson = localStorage.getItem("DSCV_LOCALSTORAGE");

var dscvJson2 = localStorage.getItem("DSCV_LOCALSTORAGEE");

if (dscvJson != null) {
    dscv = JSON.parse(dscvJson);
    for (var index = 0; index < dscv.length; index++) {
        var cv = dscv[index];
        dscv[index] = new CongViec(
            cv.tenCv,
        );
    }

    renderDSCV(dscv);
}

if (dscvJson2 != null) {
    dscvHoanThanh = JSON.parse(dscvJson2);
    for (var index = 0; index < dscvHoanThanh.length; index++) {
        var cv = dscvHoanThanh[index];
        dscvHoanThanh[index] = new CongViec(
            cv.tenCv,
        );
    }
    showDaHoanThanh(dscvHoanThanh);
}

document.getElementById("addItem").addEventListener('click', function () {

    var newCV = layThongTinTuForm();
    let isValid = validator.kiemTraRong(
        newCV.tenCv,
        "tbTask",
        "Chưa nhập nhiệm vụ!"
    );

    if (isValid) {
        dscv.push(newCV);

        var dscvJson = JSON.stringify(dscv);
        localStorage.setItem("DSCV_LOCALSTORAGE", dscvJson);
        renderDSCV(dscv);
        1;
    }

})

function xoaCongViec(id) {
    // console.log(id);

    var index = timKiemViTri(id, dscv);
    if (index != -1) {
        dscv.splice(index, 1);

        renderDSCV(dscv);
    }
    console.log('index: ', index);
}


function danhDauDaLam(id) {
    var index = timKiemViTri(id, dscv);
    if (index != -1) {
        var currentTask = dscv[index];
        dscvHoanThanh.push(currentTask);
        var dscvJson2 = JSON.stringify(dscvHoanThanh);
        localStorage.setItem("DSCV_LOCALSTORAGEE", dscvJson2);
        showDaHoanThanh(dscvHoanThanh);
        1;
    }
}


// start descending sort
document.getElementById("three").onclick = function () {
    // sort is used to sort the elements
    dscv.sort(function (a, b) {
        return b.tenCv.localeCompare(a.tenCv);
    });
    renderDSCV(dscv);
};
// end descending sort

// start Sort up
document.getElementById("two").onclick = function () {
    // console.log("yes");
    // sort is used to sort the elements
    dscv.sort(function (a, b) {
        return a.tenCv.localeCompare(b.tenCv);
    });
    renderDSCV(dscv);
};